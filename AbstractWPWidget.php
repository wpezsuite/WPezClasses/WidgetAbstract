<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 3/23/2019
 * Time: 8:52 AM
 */

namespace WPezSuite\WPezClasses\WidgetAbstract;

class AbstractWPWidget extends \WP_Widget {

	protected $_bool_active;
	protected $_new_form;
	protected $_new_update;
	protected $_new_widget;


	public function __construct( $arr_args = false ) {

		$this->_bool_active = false;
		if ( is_array($arr_args) ){

			$arr_def = [
				'title' => false,
				'glue' => '_'
			];

			$arr_args = array_merge( $arr_def, $arr_args);

			if ( is_string($arr_args['title']) ){

				$this->_bool_active = true;
				$str_glue = '_';
				if ( is_string($arr_args['glue']) ){
					$str_glue = trim($arr_args['glue']);
				}

				$str_slug = $this->makeSlug($arr_args['title'], $str_glue);
				$arr_defaults = [
					'base_id' => $str_slug,
					'classname' => $str_slug,
					'description' => ''
				];

				$arr = array_merge($arr_defaults, $arr_args);

				parent::__construct( $arr['base_id'], $arr['title'], ['classname' => $arr['classname'], 'description' => $arr['description']]);
			}
		}

	}

	protected function makeSlug( $str = '', $str_glue = '_' ) {

		$str = strtolower( trim( $str ) );
		$str = str_replace( ' ', $str_glue, $str );

		return $str;
	}

	public function getActive(){

		return $this->_bool_active;
	}

	public function setNewForm( AbstractForm $new ) {

		$this->_new_form = $new;

	}

	public function setNewUpdate( AbstractUpdate $new ) {

		$this->_new_update = $new;

	}

	public function setNewWidget( AbstractWidget $new ) {

		$this->_new_widget = $new;

	}

	public function setNewAll(AbstractForm $new_form, AbstractUpdate $new_update, AbstractWidget $new_widget ){

		$this->setNewForm($new_form);
		$this->setNewUpdate($new_update);
		$this->setNewWidget($new_widget);
	}

	// output the form
	public function form( $instance ) {

		$this->_new_form->setInstance( $instance );
		$this->_new_form->setThisWidget( $this );

		$this->_new_form->getView();


	}

	// save
	public function update( $instance_new, $instance_old ) {

		$this->_new_update->setInstanceNew( $instance_new );
		$this->_new_update->setInstanceOld( $instance_old );
		$this->_new_update->setThisWidget( $this );

		return $this->_new_update->setUpdate();
	}

	// output front-end
	public function widget( $args, $instance ) {

		$this->_new_widget->setArgs( $args );
		$this->_new_widget->setInstance( $instance );
		$this->_new_widget->setThisWidget( $this );

		$this->_new_widget->getView();

	}
}